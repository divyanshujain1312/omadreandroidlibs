package com.androidlib.Interfaces;

/**
 * Created by divyanshu.jain on 8/29/2016.
 */
public interface Constants {
    String STATUS_CODE = "statuscode";
    String MESSAGE = "message";
    int REQUEST_TIMEOUT_TIME = 30000;
}
