package com.androidlib.CustomFontViews;

/**
 * Created by divyanshu.jain on 8/17/2017.
 */

public interface Font {

    String FONT_REGULAR = "fonts/JosefinSlab-Regular.ttf";
    String FONT_BOLD = "fonts/JosefinSlab-Bold.ttf";

}
